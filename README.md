## Code challenge

To install

```sh
yarn
```

...then...

```sh
yarn start
```

...finally go to

```
http://localhost:8080/dist
```