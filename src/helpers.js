import Heap from './heap';

export const compose = (f, g) => x => f(g(x));

export const sort = data => {
  const keys = Object.keys(data);
  const obj = {};

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    obj[key] = Heap(data[key]).map(app => app.name);
  }

  return obj;
};

export const groupByHost = arr => {
  const hosts = {};

  for (let i = 0; i < arr.length; i++) {
    const current = arr[i];

    for (let y = 0; y < current.host.length; y++) {
      const host = current.host[y];

      hosts[host] ? (hosts[host][hosts[host].length] = current) : (hosts[host] = [current]);
    }
  }

  return hosts;
};

export const createReferences = data => {
  const obj = {};

  for (let i = 0; i < data.length; i++) {
    const { name } = data[i];

    if (!obj.hasOwnProperty(name)) {
      obj[name] = i;
    }
  }

  return obj;
};
