const minHeap = array => {
  for (let i = Math.floor(array.length / 2); i >= 0; i--) {
    heapify(array, i, array.length);
  }
  return array;
};

const heapify = (array, index, heapSize) => {
  const left = 2 * index + 1;
  const right = 2 * index + 2;

  let largest = index;

  if (heapSize > left && array[largest].apdex > array[left].apdex) {
    largest = left;
  }

  if (heapSize > right && array[largest].apdex > array[right].apdex) {
    largest = right;
  }

  if (largest !== index) {
    const temp = array[index];
    array[index] = array[largest];
    array[largest] = temp;
    heapify(array, largest, heapSize);
  }
};

export default function(array) {
  array = minHeap(array);

  let heapSize = array.length;
  let temp;

  for (let i = heapSize - 1; i > 0; i--) {
    temp = array[0];
    array[0] = array[i];
    array[i] = temp;
    heapSize--;
    heapify(array, 0, heapSize);
  }

  return array;
}
