import axios from 'axios';
import { compose, createReferences, sort, groupByHost } from './helpers';

const LIMIT = 25;

function Data(data) {
  this.cache = data;
  this.references = createReferences(this.cache);
  this.normalize = compose(sort, groupByHost);
  this.hosts = this.normalize(this.cache);
  this.getApp = name => this.cache[this.references[name]];
  this.getTopAppsByHost = hostName => {
    const apps = this.hosts[hostName];
    const topAppsByHost = [];

    for (let i = 0; i < LIMIT; i++) {
      topAppsByHost[topAppsByHost.length] = apps[i];
    }

    return topAppsByHost;
  };
}

export default async function() {
  const request = await axios.get('data/host-app-data.json');
  const data = new Data(request.data);

  return {
    hosts: data.hosts,
    getApp: data.getApp,
    getTopAppsByHost: data.getTopAppsByHost
  };
}
