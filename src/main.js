import './styles.scss';
import { compose } from './helpers';
import Data from './data';

const ELEMENTS = 5;
const triggers = ['name', 'apdex', 'app'];

function App() {
  this.staticMarkup = html => `<header>
      <h1>Apps by Host</h1>
      <div class="input"><input id="toggler" type="checkbox">Show as awesome grid</input></div>
    </header>
    <div id="wrapper">${html}</div></div>`;

  this.onClick = ({ target, target: { className } }) => {
    if (triggers.includes(className)) {
      const version = event.target.getAttribute('data-version') || event.target.parentNode.getAttribute('data-version');
      alert(version);
    }
  };

  this.onToggle = event => {
    if (event.target.id === 'toggler') {
      const wrapper = document.getElementById('wrapper');
      wrapper.classList.toggle('grid');
    }
  };

  this.renderer = ({ getApp, getTopAppsByHost, hosts }) => {
    let html = ``;

    const hostsKeys = Object.keys(hosts);

    for (let i = 0; i < hostsKeys.length; i++) {
      const hostName = hostsKeys[i];
      const host = getTopAppsByHost(hostName);

      html += `<div class="host"><h2>${hostName}</h2>`;

      for (let y = 0; y < ELEMENTS; y++) {
        const { version, apdex, name } = getApp(host[y]);

        html += `<div class="app" data-version="${version}">
          <span class="apdex">${apdex}</span>
          <span class="name">${name}</span>
        </div>`;
      }

      html += `</div>`;
    }

    return html;
  };

  this.render = compose(this.staticMarkup, this.renderer);
}

const init = async () => {
  const container = document.getElementById('container');
  const data = await Data();
  const app = new App();

  container.innerHTML = app.render(data);

  document.addEventListener('click', app.onClick);
  document.addEventListener('click', app.onToggle);
};

init();
